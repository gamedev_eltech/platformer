package com.shittygames.Screens;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.shittygames.Screens.Levels.TestStage;

public class MainMenu implements Screen {
    private Stage stage;
    private Table table;
    private TextButton buttonPlay,buttonExit;
    private Label heading;
    private Skin skin;
    private TextureAtlas atlas;
    private TweenManager manager;
    @Override
    public void show() {

        atlas =  new TextureAtlas("UI/button.pack");
        skin = new Skin(Gdx.files.internal("UI/menuSkin.json"),atlas);

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        table = new Table(skin);
        table.setBounds(0,Gdx.graphics.getHeight()/2,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);

        buttonExit = new TextButton("EXIT", skin);
        buttonExit.addListener(new ClickListener(){
           @Override
            public void clicked(InputEvent event,float x,float y){
               Gdx.app.exit();
           }
        });
        buttonExit.pad(10);

        buttonPlay = new TextButton("PLAY",skin);
        buttonPlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(new TestStage());
            }
        });
        buttonPlay.pad(10);

        heading = new Label("Tha gemu",skin);
        heading.setFontScale(3);

        table.add(heading);
        table.getCell(heading).spaceBottom(100);
        table.row();
        table.add(buttonPlay);
        table.getCell(buttonPlay).spaceBottom(20);
        table.row();
        table.add(buttonExit);
        //table.debug();
        stage.addActor(table);

        manager = new TweenManager();
        Tween.registerAccessor(Actor.class,new ActorAccessor());
        Timeline.createSequence().beginSequence()
                .push(Tween.to(heading,ActorAccessor.RGB,0.5f).target(1,0,0))
                .push(Tween.to(heading,ActorAccessor.RGB,0.5f).target(1,1,0))
                .push(Tween.to(heading,ActorAccessor.RGB,.5f).target(0,1,0))
                .push(Tween.to(heading,ActorAccessor.RGB,0.5f).target(0,1,1))
                .push(Tween.to(heading,ActorAccessor.RGB,.5f).target(0,0,1))
                .push(Tween.to(heading,ActorAccessor.RGB,0.5f).target(1,0,1))
                .end().repeat(Tween.INFINITY,0).start(manager);
        Timeline.createSequence()
        .beginSequence()
                .push(Tween.set(buttonPlay,ActorAccessor.ALPHA).target(0))
                .push(Tween.set(buttonExit,ActorAccessor.ALPHA).target(0))
                .push(Tween.from(heading,ActorAccessor.ALPHA,0.5f).target(0))
                .push(Tween.to(buttonPlay,ActorAccessor.ALPHA,0.5f).target(1))
                .push(Tween.to(buttonExit,ActorAccessor.ALPHA,0.5f).target(1))
                .end().start(manager);
        Tween.from(table,ActorAccessor.ALPHA,.5f).target(0);
        Tween.from(table,ActorAccessor.Y,.5f).target(Gdx.graphics.getHeight()).start(manager);


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        manager.update(delta);
        stage.act(delta);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        atlas.dispose();
        skin.dispose();
    }

    private class ActorAccessor implements TweenAccessor<Actor> {
        public static final int Y=0,RGB = 1,ALPHA =2;

        @Override
        public int getValues(Actor target, int tweenType, float[] returnValues) {
            switch (tweenType){
                case Y:
                    returnValues[0] = target.getY();
                    return 1;
                case RGB:
                    returnValues[0] = target.getColor().r;
                    returnValues[1] = target.getColor().g;
                    returnValues[2] = target.getColor().b;
                    return 3;
                case ALPHA:
                    returnValues[0]=target.getColor().a;
                    return 1;
                default:
                    assert false;
                    return -1;
            }
        }

        @Override
        public void setValues(Actor target, int tweenType, float[] newValues) {
            switch (tweenType){
                case Y:
                    target.setY(newValues[0]);
                    break;
                case RGB:
                    target.setColor(newValues[0],newValues[1],newValues[2],target.getColor().a);
                    break;
                case ALPHA:
                    target.setColor(target.getColor().r,target.getColor().g,target.getColor().b,newValues[0]);
                    break;
                default:
                    assert false;
            }
        }
    }
}
