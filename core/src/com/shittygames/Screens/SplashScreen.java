package com.shittygames.Screens;

import aurelienribon.tweenengine.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.Screens.Levels.TestStage;

public class SplashScreen implements Screen {
    private Sprite splash;
    private SpriteBatch batch;
    private TweenManager tweenManager;
    @Override
    public void show() {
        splash = new Sprite(new Texture("splash.jpg"));
        batch = new SpriteBatch();
        splash.setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        tweenManager = new TweenManager();
        Tween.registerAccessor(Sprite.class,new SpriteAccessor());
        Tween.set(splash,SpriteAccessor.ALPHA).target(0).start(tweenManager);
        Tween.to(splash,SpriteAccessor.ALPHA,1)
                .target(1)
                .repeatYoyo(1,0.5f)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        ((Game)Gdx.app.getApplicationListener()).setScreen(new MainMenu());
                    }
                })
                .start(tweenManager);
        //Tween.to(splash,SpriteAccessor.ALPHA,2).target(0).delay(2).start(tweenManager);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,1,1,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        tweenManager.update(delta);

        batch.begin();
        splash.draw(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        splash.getTexture().dispose();
    }
    private class SpriteAccessor implements TweenAccessor<Sprite> {
        public static final int ALPHA =0;
        @Override
        public int getValues(Sprite sprite, int i, float[] floats) {
            switch (i){
                case ALPHA:
                    floats[0] = sprite.getColor().a;
                    return 1;
                default:
                    assert false;
                    return -1;
            }
        }

        @Override
        public void setValues(Sprite sprite, int i, float[] floats) {
            switch (i){
                case ALPHA:
                    sprite.setColor(sprite.getColor().r,sprite.getColor().g,sprite.getColor().b,floats[0]);
                    break;
                default:
                    assert false;

            }
        }
    }
}
