package com.shittygames.Screens.Levels;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.shittygames.Managers.*;
import com.shittygames.actors.Background;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.Mobs.TestBot;
import com.shittygames.actors.StaticObject;
import java.io.FileNotFoundException;
import static com.shittygames.Managers.ActorManager.Add;
import static com.shittygames.Managers.ActorManager.get;
;

public class TestStage implements Screen{
	public static final float G = 40;
	public static final int LANDSCAPE_LAYER=1,MOB_LAYER=2,PLAYER_LAYER=3,FOREGROUND_LAYER=4;
	private SpriteBatch batch;
	//public static OrthographicCamera camera;
	private float camDx, delta;
	private Vector3 pos;
	private BitmapFont font;

	@Override
	public void show() {
		try{
			TextureManager.Initialize();}catch (FileNotFoundException e){e.printStackTrace();}
		batch = new SpriteBatch();
		ActorManager.Initialize();
		AudioManager.Initialize();
		Add(new Background(6,"background"));
		//Add(new Background(4,"clouds"));
		//Add(new Mob(400,200,64,64,2,0,true,"player",PLAYER_LAYER,123));
		Add(new Player(400,200,64));
		Add(new TestBot(200,200,63));
		/*Add(new Mob(0,200,140,180,1,0,false,"bot",MOB_LAYER,1));
		((Mob)get("bot")).setPatrol(0,400,false);
		Add(new Mob(100,200,140,180,1,0,false,"bot1",MOB_LAYER,1));
		Add(new Mob(400,600,140,180,1,0,false,"bot2",MOB_LAYER,1));
		((Mob)get("bot1")).setAggression("bot2");
		((Mob)get("bot2")).setAggression("bot1");
		*/
		Add(new StaticObject(-1000,-128,2000,192,5,0,"ground",LANDSCAPE_LAYER));
		Add(new StaticObject(600,64,100,100,5,2,"box",LANDSCAPE_LAYER));
		//Add(new StaticObject(700,50,1000,300,5,0,true,"wall",LANDSCAPE_LAYER));
		Add(new StaticObject(-1050,-500,50,564,5,0,"leftwall",LANDSCAPE_LAYER));
		Add(new StaticObject(1000,-360,13800,1080,15,1,"wall",LANDSCAPE_LAYER));
		Add(new StaticObject(1000,-130,13800,60,12,0,"floor",LANDSCAPE_LAYER));
		Add(new StaticObject(1000,-360,13800,230,5,0,"rocks",1));
		Add(new StaticObject(1200,-70,128,116,7,2,"shit",2));
		Add(new StaticObject(1400,-70,126,108,8,2,"shit",2));
		Add(new StaticObject(1600,-70,178,140,9,2,"shit",2));
		Add(new StaticObject(1800,-70,146,160,10,2,"shit",2));
		Add(new StaticObject(2000,-70,126,164,11,2,"shit",2));
		Add(new StaticObject(2200,-70,108,136,13,2,"shit",2));
		Add(new StaticObject(2400,-70,56,48,14,2,"shit",2));
		//Add(new Mob(0,130,64,64,2,192,true,"p",MOB_LAYER,0));
		//((Mob)get("p")).setParams(5,0,6,0,1,1);
		((StaticObject)get("ground")).setPaveMode(320,180);
		((StaticObject)get("box")).setPaveMode(320,180);
		((StaticObject)get("wall")).setPaveMode(128,128);
		((StaticObject)get("floor")).setPaveMode(574,60);
		((StaticObject)get("rocks")).setPaveMode(320,180);
		//((StaticObject)get("wall")).setPaveMode(320,180);
		((StaticObject)get("leftwall")).setPaveMode(320,180);
		//CameraAdapter.Camfollow("player1");
		ScriptManager.LoadResources();
		CameraManager.InitCamera();
		CameraManager.CamFollow("player1");
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		font = new BitmapFont();
		font.setColor(Color.BLACK);
		//((Background)get("background")).setPaveMode(240,720);
	}

	@Override
	public void render(float delta) {
		//camera.update();
		CameraManager.CamUpdate(delta);
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		ScriptManager.UserInput();
		ScriptManager.ScriptUpdate();
		ActorManager.Step(batch,delta);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}
}



