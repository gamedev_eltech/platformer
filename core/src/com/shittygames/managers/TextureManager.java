package com.shittygames.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Consumer;


/**
 * Created by user-00 on 16.09.17.
 */
public class TextureManager { // NEED REWORK
    private static TextureRegion[] textures;
    /*
    data example:
    5 64 20 <- pos params: frames count, frame width, each frame duration *100
    poses: idle, move, jump, fall, attack, hit, death, special1, sit, special2, etc
    */
    @Deprecated
    public static void Initialize() throws FileNotFoundException {
        Scanner scanner;
        try {
            scanner = new Scanner(new FileInputStream("textures.list"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            scanner = null;
        }
        textures = new TextureRegion[scanner.nextInt()];
        for (int i = 0; i < textures.length; i++) {
            textures[i] = new TextureRegion(new Texture(scanner.next()+".png"), scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        }
        scanner.close();
    }
    @Deprecated
    public static TextureRegion getTexture(int texture) {
        return textures[texture];
    }
    @Deprecated
    public static TextureRegion[] getTextures(String[] files){
        TextureRegion[] regions = new TextureRegion[files.length-1];
        //TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(files[0]+"/"+files[0]+".pack"));
        //for(int i=1;i<files.length;i++){
        //    regions[i-1] = atlas.findRegion(files[i]);
        //}
        //atlas.dispose();
        for(int i=1;i<files.length;i++){
            regions[i-1] = new TextureRegion(new Texture(Gdx.files.internal(files[0]+'/'+files[i]+".png")));
            //System.out.println(regions[i-1].getTexture().toString()+ "is Loaded");
        }
        return regions;
    }
    private static ArrayList<TextureAtlas> textureList;
    private static Array<TextureAtlas.AtlasRegion> regionList;
    public static void loadTextures(String file){
        if(regionList==null)regionList = new Array<>();
        Scanner scanner;
        try{
            scanner = new Scanner(new FileInputStream(file));
        }catch (FileNotFoundException e){e.printStackTrace();scanner = null;}
        textureList = new ArrayList<>();
        //TextureAtlas.AtlasRegion tmp[];
        int i=0;
        while(scanner.hasNext()){
            textureList.add(new TextureAtlas(scanner.next()));
            //tmp = textureList.get(i).getRegi;
            //for(int j=0;j<tmp.length;j++)regionList.add(tmp[i]);
            regionList.addAll(textureList.get(i).getRegions());
            i++;
        }
        scanner.close();
    }
    public static void disposeTextures(){
        for (TextureAtlas a: textureList) {
            a.dispose();
        }
        textureList.clear();
        regionList.clear();
    }
    public static int getRegionIndex(String regionName){
        for (int i=0;i<regionList.size;i++) {
            if(regionList.get(i).name.equals(regionName)) return i;
        }
        System.out.println(regionName + " Not found");
        return -1;
    }
    public static TextureRegion getRegion(int regionIndex){
        return regionList.get(regionIndex);
    }

}
