package com.shittygames.managers;

import com.badlogic.gdx.Gdx;
import com.shittygames.actors.Explosion;
import com.shittygames.actors.Fireball;
import com.shittygames.actors.Mobs.Mob;
import com.shittygames.actors.Mobs.Mob.*;
import com.shittygames.actors.MyActor;

/*
 * Created by user-00 on 15.09.17.
 * 0 - absolute wall
 * 1 - decorations - never calls if main
 * 2 - static box
 * 3 - movable obj
 * 4 - mob
 * 5 - player
 * 6 - damage box - enemy
 * 7 - dmg box - player
 * 8 - dmg box - all
 * 9 - fireball
 */
public class CollisionManager {
    public static final int ABS_WALL=0,DECORATION=1,BOX=2,MOVABLE=3,
            MOB=4,PLAYER=5,DMGBOX_ENEMY=6,DMGBOX_PLAYER=7,DMGBOX_ALL=8,FIREBALL=9;
    public static void ReactOn(MyActor MainActor, MyActor CollidedObj, byte posInfo) {
        //posInfo : 0 - main upper than obj
        //          1 -  right
        //          2 - below
        //          3 - left
        //          4 - main inside the obj
        //          5 - obj inside the main
        switch (CollidedObj.getScript()){
            case ABS_WALL:{
                MainActor.vx = 0;
                if(MainActor.getScript()==FIREBALL){
                    System.out.println("collision");
                    ((Fireball)CollidedObj).eventOnHit();return;}

                if (posInfo == 0) {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) {((Mob) MainActor).gndvx = CollidedObj.vx;
                    ((Mob) MainActor).pos = airPos.GROUNDED;}
                    MainActor.vy = 0;
                    MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
                } else if (posInfo == 1) {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) ((Mob) MainActor).vl = 0;
                    MainActor.vx = (MainActor.vx < 0 ? 0 : MainActor.vx);
                    MainActor.setX(CollidedObj.getX() + CollidedObj.getWidth());
                } else if (posInfo == 2) {
                    MainActor.vy = MainActor.vy > 0 ? 0 : MainActor.vy;
                    MainActor.setY(CollidedObj.getY() - MainActor.getHeight() - 1);

                } else {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) ((Mob) MainActor).vr = 0;
                    MainActor.vx = (MainActor.vx > 0 ? 0 : MainActor.vx);
                    MainActor.setX(CollidedObj.getX() - MainActor.getWidth());
                }
                break;
            }
            case BOX:{
                MainActor.vx = 0;
                if(posInfo==0&&MainActor.vy<0){
                    MainActor.vy = 0;
                    MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
                    if(MainActor.getScript() == 5|| MainActor.getScript()==6){
                    ((Mob) MainActor).gndvx = CollidedObj.vx;
                    ((Mob) MainActor).pos = airPos.GROUNDED;
                    }
                }
                break;
            }
            case 3:{

            }
            case 4:{

            }
            case 5:{

            }
            case 6:{

            }
            case 7:{

            }
            case 8:{

            }
            case FIREBALL:{
               /* System.out.println("collision");
                if(MainActor.getScript()==ABS_WALL)
                    ((Fireball)CollidedObj).eventOnHit();*/
            }
        }/*
        if (MainActor.isCollides() && CollidedObj.isCollides()) {
            MainActor.vx =0;
            if (posInfo == 0) {
                if (MainActor.getScript() == 0) ((Mob) MainActor).gndvx = CollidedObj.vx;
                ((Mob) MainActor).pos = airPos.GROUNDED;
                MainActor.vy = 0;
                MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
            } else if (posInfo == 1) {
                if (MainActor.getScript() == 0) ((Mob) MainActor).vl = 0;
                MainActor.vx = (MainActor.vx < 0 ? 0 : MainActor.vx);
                MainActor.setX(CollidedObj.getX() + CollidedObj.getWidth());
            } else if (posInfo == 2) {
                MainActor.vy = MainActor.vy > 0 ? 0 : MainActor.vy;
                MainActor.setY(CollidedObj.getY() - MainActor.getHeight() - 1);

            } else {
                if (MainActor.getScript() == 0) ((Mob) MainActor).vr = 0;
                MainActor.vx = (MainActor.vx > 0 ? 0 : MainActor.vx);
                MainActor.setX(CollidedObj.getX() - MainActor.getWidth());
            }
        }
        switch (CollidedObj.getScript()) {
            case 2: {
                if (posInfo == 0) {
                    MainActor.vx = CollidedObj.vx;
                    if (CollidedObj.vy <= 0) MainActor.vy = CollidedObj.vy;
                }
                break;
            }
            case 192:{
                if(MainActor.getScript()==0){
                    //AudioAdapter.playMusic(0);
                    ((Mob)CollidedObj).HP-=1;
                    ((Mob)MainActor).pos = airPos.GROUNDED;
                    ((Mob)MainActor).jump();
                }
            }
        }
         */
    }

}
