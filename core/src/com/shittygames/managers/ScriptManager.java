package com.shittygames.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.shittygames.actors.Explosion;
import com.shittygames.actors.Mobs.Mob;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.Mobs.TestBot;
import com.shittygames.utils.Trajectory;

import static com.shittygames.managers.ActorManager.get;
import static com.shittygames.managers.ActorManager.getLast;
import static com.shittygames.managers.ActorManager.getPlayer;

public class ScriptManager {
    static float timer = 0;
    public static void LoadResources(){
        Player.LoadAssets();
        TestBot.LoadAssets();
    }
    public static void UserInput(){
        /*if(Gdx.input.isKeyPressed(Input.Keys.NUM_1))ActorAdapter.Add(new Explosion(400,300,32,32,1,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_2))ActorAdapter.Add(new Explosion(400,300,64,64,2,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_3))ActorAdapter.Add(new Explosion(400,300,128,80,3,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_4))ActorAdapter.Add(new Explosion(400,300,128,128,4,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_5))ActorAdapter.Add(new Explosion(400,300,192,192,5,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_6))ActorAdapter.Add(new Explosion(400,300,48,48,6,100,0));
        */
        timer += Gdx.graphics.getDeltaTime();
        if(false&&Gdx.input.isKeyPressed(Input.Keys.NUM_1)&&timer>0.3f){
            timer = 0;
            ActorManager.Add(new Explosion(getPlayer().getX(),
                getPlayer().getY(),64,64,1,15,0));
            ((Explosion)getLast()).setTrajectory(new Trajectory(getPlayer().getX(),getPlayer().getY(),getPlayer().Right) {
                @Override
                public void updateX() {
                    x = 800*t + x0;
                }

                @Override
                public void updateY() {
                    y = 40*(float)Math.sin(10*t)+y0;
                }
            });
            ((Mob)getPlayer()).MP-=25;
        }
    }
    public static void ScriptUpdate(){
       /*         if(getPlayer().getX()<=-900&&!getPlayer().isMoving()){
            //System.out.println("call");
            get("leftwall").moveTo(-1050,0,.5f);}
        if(getPlayer().getX()>-900&&!get("leftwall").isMoving()){
          get("leftwall").moveTo(-1050,-500,.5f);}
          */
    }
}
