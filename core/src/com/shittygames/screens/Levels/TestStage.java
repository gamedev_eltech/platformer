package com.shittygames.screens.Levels;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.shittygames.actors.Background;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.Mobs.TestBot;
import com.shittygames.actors.RoamingObject;
import com.shittygames.actors.StaticObject;
import com.shittygames.managers.*;
import com.shittygames.utils.AnimatedTexture;
import com.shittygames.utils.Trajectory;

import static com.shittygames.managers.ActorManager.*;
import static com.shittygames.managers.CollisionManager.ABS_WALL;


public class TestStage implements Screen{
    public static final String NONAME=null;
	public static final float G = 40;
	public static final int LANDSCAPE_LAYER=1,MOB_LAYER=2,PLAYER_LAYER=3,FOREGROUND_LAYER=4;
	private SpriteBatch batch;
	//public static OrthographicCamera camera;
	private float camDx, delta;
	private Vector3 pos;
	private BitmapFont font;

	@Override
	public void show() {
		TextureManager.loadTextures("textures.list");
		batch = new SpriteBatch();
		CameraManager.InitCamera();
		ActorManager.Initialize();
		AudioManager.Initialize();
		ScriptManager.LoadResources();
		//Add(new Background(6));
		AddPlayer(new Player(400,200,4));
		Add(new StaticObject(0,0,4800,128,"stone",0,null,1));
		((StaticObject)getLast()).setPaveMode(64,64);
		Add(new Background("back"));
		((Background)getLast()).setParallax(.1f,0);
		Add(new RoamingObject(64,64,"explosion-1",8,0.1f,AnimatedTexture.PlayMode.LOOP_PINGPONG,1,null,2));
		((RoamingObject)getLast()).setTrajectory(new Trajectory(360,360,true) {
			@Override
			public void updateX() {
				x = -(float)(200*Math.sin(t))+x0;
			}

			@Override
			public void updateY() {
				y = (float)(200*Math.cos(t))+y0;
			}
		});
		Add(new StaticObject(600,128,128,128,"explosion-4",12,0.1f, AnimatedTexture.PlayMode.NORMAL,1,null,10));
		Add(new StaticObject(1000,142,64,100,"stone",ABS_WALL,null,2));
		CameraManager.CamFollow(getPlayer());
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		font = new BitmapFont();
		font.setColor(Color.BLACK);
	}

	@Override
	public void render(float delta) {
		//camera.update();
		CameraManager.CamUpdate();
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		ScriptManager.UserInput();
		ScriptManager.ScriptUpdate();
		ActorManager.Step(batch,delta);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		TextureManager.disposeTextures();
		AudioManager.dispose();
	}
}



