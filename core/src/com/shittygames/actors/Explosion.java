package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.managers.TextureManager;
import com.shittygames.utils.Trajectory;

public class Explosion extends MyActor {
    private static final int data[][]= {{32,32,300},{64,64,100},{128,80,100},{128,128,100},{192,192,100},{48,48,100}};
    //textureWidth,textureHeight,frameDuration (mills)
    private static final String names[] = {"Explosion","explosion-1","explosion-2",
            "explosion-3","explosion-4","explosion-5","explosion-6"};
    private float timer;
    private static TextureRegion[][] frames;
    private int a_ptr;
    private Trajectory trajectory=null;
    public Explosion(float x, float y, float width, float height, int Exp_num,int layer,float delay) {
        super(x, y, width, height,null, 3, "", layer);
        this.timer = -delay;
        a_ptr = Exp_num-1;
    }
    public void setTrajectory(Trajectory trajectory){
        this. trajectory = trajectory;
    }
    @Override
    public void update(float delta) {
        super.update(delta);
        timer+=delta;
        if(timer>0&&trajectory!=null){
            trajectory.update(delta);
            this.x = trajectory.x;
            this.y = trajectory.y;
        }
        if(timer*1000>data[a_ptr][2]*frames[a_ptr].length)this.Destroy();
    }
    @Override
    public void draw(SpriteBatch batch){
        if(timer>=0)batch.draw(frames[a_ptr][(int)(timer*1000/data[a_ptr][2])%frames[a_ptr].length],x,y,width,height);
    }

    public static void LoadAssets(){
        frames = new TextureRegion[names.length-1][];
        TextureRegion[] res = TextureManager.getTextures(names);
        for(int i=0;i<res.length;i++){
            frames[i] = res[i].split(data[i][0],data[i][1])[0];
        }
    }
}
