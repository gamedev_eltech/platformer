package com.shittygames.actors;

import com.shittygames.utils.AnimatedTexture;
import com.shittygames.utils.Trajectory;

/**
 * Created by admin on 31.08.2017.
 */
public class RoamingObject extends MyActor {
    float[] dots; // pairs of coords like {x1,y1,x2,y2 . . . etc} size>=4;
    float secs;
    int dot = 0;
    private Trajectory trajectory;

    public RoamingObject(float width, float height, String texture, int script, String name, int layer) {
        super(0, 0, width, height, texture, script, name, layer);
    }
    public RoamingObject(float width, float height, String texture, int frameCount, float duration, AnimatedTexture.PlayMode playMode, int script, String name, int layer){
        super(0,0,width,height,texture,frameCount,duration,playMode,script,name,layer);
    }

    public void setTrajectory(Trajectory trajectory) {
        this.trajectory = trajectory;
    }

    public void setPath(float secs, float[] dots) {
        x = dots[0];
        y = dots[1];
        this.dots = dots;
        this.secs = secs;
        vx = (dots[dot + 2] - dots[dot]) / secs;
        vy = (dots[dot + 3] - dots[dot + 1]) / secs;
        dot = 2;
    }

    public void update(float delta) {
        texture.update(delta);
        if (trajectory != null) {
            trajectory.update(delta);
            this.x = trajectory.x;
            this.y = trajectory.y;
        } else {
            if (Math.abs(dots[dot] - x) <= Math.abs(vx * delta) && Math.abs(dots[dot + 1] - y) <= Math.abs(vy * delta)) {
                x = dots[dot];
                y = dots[dot + 1];
            } else {
                x += vx * delta;
                y += vy * delta;
            }

            if (x == dots[dot] & y == dots[dot + 1]) {
                if (dot == dots.length - 2) {
                    dot = 0;
                } else {
                    dot += 2;
                }
                vx = (dots[dot] - x) / secs;
                vy = (dots[dot + 1] - y) / secs;
            }
        }
    }
}
