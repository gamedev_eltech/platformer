package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.managers.TextureManager;
import com.shittygames.utils.AnimatedTexture;

/**
 * Created by admin on 31.08.2017.
 */
public class StaticObject extends MyActor {
    enum DrawMode {PAVE, STRETCH}

    private float tileWidth, tileHeight;
    private DrawMode mode = DrawMode.STRETCH;

    public StaticObject(float x, float y, float width, float height, String texture, int script, String name, int layer) {
        super(x, y, width, height, texture, script, name, layer);
    }
    public StaticObject(float x, float y, float width, float height, String texture, int frameCount, float frameDuration, AnimatedTexture.PlayMode playMode,int script,String name,int layer){
        super(x,y,width,height,texture,frameCount,frameDuration,playMode,script,name,layer);
    }

    public void setPaveMode(float tileWidth, float tileHeight) {//ЗАМОСТИТЬ
        mode = DrawMode.PAVE;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    public void setStretchMode() {
        mode = DrawMode.STRETCH;
    }

    @Override
    public void draw(SpriteBatch batch) {
        if (mode == DrawMode.STRETCH) batch.draw(texture.getFrame(), x, y, width, height);
        else {
            int row = (int) (height / tileHeight), col = (int) (width / tileWidth);
            row += (row * tileHeight < height) ? 1 : 0;
            col += (col * tileWidth < width) ? 1 : 0;
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    TextureRegion reg = texture.getFrame();
                    batch.draw(reg.getTexture(),x+tileWidth*j,
                            y+tileHeight*i,
                            (int)(tileWidth * (j + 1) > width ?
                                    width - tileWidth * j: tileWidth),
                            (int)(tileHeight * (i + 1) > height ?
                                    height - tileHeight * i : tileHeight),
                            reg.getRegionX(),reg.getRegionY(),
                            (int)(tileWidth * (j + 1) > width ?
                                    reg.getRegionWidth()*(width-tileWidth*j)/tileWidth:reg.getRegionWidth()),
                            (int)(tileHeight * (i + 1) > height ?
                                    reg.getRegionHeight()*(height-tileHeight*i)/tileHeight:reg.getRegionHeight()),
                            false,false);
                    //(int)(reg.getRegionWidth()*(j+1)>width?width-reg.getRegionWidth()*j:reg.getRegionWidth()),
                            //(int)(reg.getRegionHeight()*(i+1)>height?height-reg.getRegionHeight()*i:reg.getRegionHeight()));
                    //batch.draw(TextureAdapter.getTexture(texture), x + tileWidth * j,
                       //     y + tileHeight * i, tileWidth * (j + 1) > width ? width - tileWidth * j: tileWidth,
                           // tileHeight * (i + 1) > height ? height - tileHeight * i : tileHeight);
                }
            }
        }
    }
}
