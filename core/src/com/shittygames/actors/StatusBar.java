package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.TextureManager;
import com.shittygames.actors.Mobs.Mob;

public class StatusBar extends AttachedObject {
    private static final float dXMul=1,dYMul=1.1f,wMul=0.7f;
    boolean isEnemy;
    public StatusBar(Mob mob,boolean isEnemy) {
        super(0, mob.y*dYMul,mob.width*wMul, 10, "whitepix", 1, "stb", mob.layer);
        this.origin = mob;
        this.isEnemy = isEnemy;
    }
    @Override
    public void update(float delta){
        super.update(delta);
        if(origin.isDestroying())this.Destroy();
        }
    @Override
    public void draw(SpriteBatch batch){
        if(isEnemy)batch.setColor(1,0,0,1);
        else batch.setColor(0,1,0,1);
        batch.draw(texture.getFrame(),x,y+5,width*((Mob)origin).HP/((Mob)origin).maxHP,5);
        batch.setColor(0,0,1,1);
        if(((Mob)origin).MP>0)batch.draw(texture.getFrame(),x,y,width*((Mob)origin).MP/((Mob)origin).maxMP,5);
        batch.setColor(1,1,1,1);
    }
}
