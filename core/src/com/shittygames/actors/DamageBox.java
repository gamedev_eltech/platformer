package com.shittygames.actors;

import com.badlogic.gdx.Gdx;
import com.shittygames.actors.MyActor;

public class DamageBox extends MyActor {
    public static final float ONE_FRAME = 0.16f;
    public float damage,delay=0,duration=ONE_FRAME;
    public static final int DMG_ALL=8,DMG_PLAYER=7,DMG_ENEMY=6;
    public DamageBox(float x,float y, float width, float height,float damage, int DamageType) {
        super(x, y, width, height, null, DamageType, null, 10);
        this.damage = damage;
    }
    public DamageBox(float x,float y, float width, float height,float damage, float duration,float delay,int DamageType){
        super(x,y,width,height,null,DamageType,null,10);
        this.damage = damage;
        this.delay = delay;
        this.duration = duration;
    }
    @Override
    public void update(float delta) {
        if(delay<0){
            duration-=delay;
            if(duration<0)
                this.Destroy();
        }
        else delay-=delta;
    }
    public float getDamage(){
        if(duration> Gdx.graphics.getDeltaTime())
            return damage*Gdx.graphics.getDeltaTime();
        else return damage*duration;
    }
}
