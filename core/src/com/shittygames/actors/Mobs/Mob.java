package com.shittygames.actors.Mobs;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.actors.MyActor;

import static com.shittygames.actors.Mobs.Mob.airPos.GROUNDED;
import static com.shittygames.actors.Mobs.Mob.behaviorMod.IDLE;

public abstract class Mob extends MyActor {
    //static final int data[][] = {};
    private static final int SPRITE_HEIGHT=64;
    public enum airPos{GROUNDED,FALLING,JUMPING};
    enum behaviorMod{IDLE,PATROL, AGGRESSION,FOLLOW,PLAYER};
    public static final float V_HOR = 400,V_JUMP =1000;
    public float HP=1,MP=0,maxHP=1,maxMP=0,vl,vr,gndvx,timer=0,horMul=1,jMul=1;
    private float patrolLeft,patrolRight,maxDist;
    private byte patrol_dir=0;
    private Mob target=null;
    public airPos pos;
    private behaviorMod mod;
    public boolean Right =true;

    private TextureRegion frame;
    private int stat=0,color=0xffffffff;
    public Mob(float x, float y, float width, float height, String texture, int script, String name, int layer) {
        super(x, y, width, height, texture, script, name, layer);

        pos = GROUNDED;
        mod = IDLE;
    }
    public void setParams(float HP,float MP,float maxHP,float maxMP,float v_hor_mult,float jump_mult){
        this.HP = HP;
        this.MP = MP;
        this.maxHP = maxHP;
        this.maxMP = maxMP;
        this.horMul = v_hor_mult;
        this.jMul = jump_mult;
    }


    @Override
    public abstract void update(float delta);
        /*vl = vr = V_HOR*jMul;
        vx = 0;
        vy -= MyGdxGame.G;
        gndvx = 0;
        pos = FALLING;
        CheckTouches(this,delta);
        switch (mod) {
            case IDLE: {
                break;
            }
            case FOLLOW: {
                if (this.getX() <= target.getX() - maxDist) moveRight();
                else if (this.getX() >= target.getX() + maxDist) moveLeft();
                if (this.getY() < target.getY()) jump();
                break;
            }
            case PATROL: {
                if (patrol_dir == 1) {
                    moveLeft();
                    if (vr == 0) jump();
                    if (this.getX() <= patrolLeft) patrol_dir = 0;
                } else {
                    moveRight();
                    if (vl == 0) jump();
                    if (this.getX() >= patrolRight) patrol_dir = 1;
                }
                break;
            }
            case AGGRESSION: {
                if (target.getX() < getX()) moveLeft();
                else moveRight();
                if (vl == 0 || vr == 0) jump();
                break;
            }
            case PLAYER: {
                if (Gdx.input.isKeyPressed(Input.Keys.A)) moveLeft();
                if (Gdx.input.isKeyPressed(Input.Keys.D)) moveRight();
                if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) jump();
                break;
            }
        }
            if(HP==0)this.Destroy();
            x += vx*delta;
            y += vy*delta;
          // if(vx!=0)
               timer +=delta;
           // if(y<-2000)Destroy();
           */
    @Override
    abstract public void draw(SpriteBatch batch);
        /*
    updFrame();
    //batch.setColor(color);
    batch.draw(frame,movR?x:x+width,y,!movR?-width:width,height);
    batch.setColor(1,1,1,1);
    }
    abstract public void setPatrol(float left,float right,boolean isCollides);
     /*   {mod = PATROL;
        patrolLeft = left;
        patrolRight = right;
        setCollision(isCollides);
        */
         public void setIdle(){}
    //{mod = IDLE;setCollision(false);target = null;}
    public void setAggression(String mobName){}
    //{mod = AGGRESSION;setCollision(true);target = (Mob)get(mobName);}
    public void setFollow(Mob target,float maxDist){}
    //{mod=FOLLOW;this.maxDist = maxDist;this.target = target;}
    abstract public void jump();
    //{if(pos==GROUNDED){vy+=V_JUMP*jMul;pos = JUMPING;}}
    abstract void moveLeft();
    //{vx=gndvx - vl;movR = false;}
    abstract void moveRight();
    //{vx=gndvx + vr;movR = true;}
    public void attack(){}
    public void sit(){}
    public void special(){}
    /*{
        if(drawData[0]==-1)frame = TextureAdapter.getTexture(texture);
        else{
            if(drawData.length>stat)
            frame = new TextureRegion(TextureAdapter.getTexture(texture),(drawData[stat+1]*((int)(timer/drawData[stat+2]*100)%drawData[stat])), SPRITE_HEIGHT*stat/3,drawData[stat+1],SPRITE_HEIGHT);
            else {stat=0;updFrame();}
        }
    }
    */
}
