package com.shittygames.actors.Mobs;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.managers.ActorManager;
import com.shittygames.managers.TextureManager;
import com.shittygames.actors.StatusBar;

import static com.shittygames.screens.Levels.TestStage.G;
import static com.shittygames.actors.Mobs.Mob.airPos.FALLING;
import static com.shittygames.actors.Mobs.Mob.airPos.GROUNDED;
import static com.shittygames.actors.Mobs.Mob.airPos.JUMPING;

public class TestBot extends Mob {
    static final int data[][] = {{32, 48, 900, 1, 96, 144}, {60, 80, 120, 1, 120, 135}, {60, 80, 100, 5, 135, 165}};
    //textureWidth,textureHeight,delay,playMode,drawWidth,drawHeight
    static final String name[] = {"enemy-idle", "enemy-walk", "enemy-jump"};
    //package,filenames without extension
    private static final float V_HOR = 400, V_JUMP = 1000;
    private static final int def_w = 92, def_h = 128, script = 5;
    private static int name_counter = 1;
    private Mob target;
    /*
    playMode:
    1 LOOP
    2 LOOP_PINGPONG
    3 LOOP_RANDOM
    4 LOOP_REVERSED
    5 NORMAL
    6 REVERSED
     */

    static Animation mov[];
    int mov_ptr = 0;

    public TestBot(float x, float y, String name, int layer) {
        super(x, y, def_w, def_h, null, script, name, layer);
        target = (Mob) ActorManager.get("player1");
        setParams(100, 0, 100, 0, 1, 1);

        ActorManager.Add(new StatusBar(this, true));
    }

    public TestBot(float x, float y, int layer) {
        super(x, y, def_w, def_h, null, script, "bot" + name_counter++, layer);
        target = (Mob) ActorManager.getPlayer();
        setParams(100, 0, 100, 0, 1, 1);

        ActorManager.Add(new StatusBar(this, true));
    }

    @Override
    public void update(float delta) {
        vl = vr = V_HOR * horMul;
        vy -= G;
        gndvx = 0;
        pos = airPos.FALLING;
        ActorManager.CheckTouches(this, delta);
        //if (Gdx.input.isKeyPressed(Input.Keys.A)) moveLeft();
        if (target.getX() + target.getWidth() < this.x) moveLeft();
        //if (Gdx.input.isKeyPressed(Input.Keys.D)) moveRight();
        if (target.getX() > this.x + this.width) moveRight();
        //if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) jump();
        if (target.getY() > this.y + this.height) jump();
        //if(pos==GROUNDED&&!Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)&&mov_ptr!=0){mov_ptr=0;timer=0;}
        if (pos == GROUNDED && vx == 0) {
            mov_ptr = 0;
            timer = 0;
        }
        if (HP == 0) this.Destroy();
        x += vx * delta;
        y += vy * delta;
        // if(vx!=0)
        timer += delta;
        if (HP < 0) this.Destroy();
    }

    @Override
    public void draw(SpriteBatch batch) {
        // batch.draw(mov[mov_ptr].getKeyFrame(timer),getX(),getY(),data[mov_ptr][4],data[mov_ptr][5]);
        batch.setColor(0, 1, 1, 1);
        batch.draw(mov[mov_ptr].getKeyFrame(timer), Right ? x : x + data[mov_ptr][4]
                , y, data[mov_ptr][4] * (!Right ? -1 : 1), data[mov_ptr][5]);
        batch.setColor(1, 1, 1, 1);
    }

    @Override
    public void jump() {
        if (pos == GROUNDED) {
            vy += V_JUMP * jMul;
            pos = JUMPING;
            mov_ptr = 2;
            timer = 0;
        }
    }

    @Override
    void moveLeft() {
        vx = gndvx - vl;
        Right = false;
        if (vl > 0 && mov_ptr != 1 && pos != FALLING) {
            mov_ptr = 1;
            timer = 0;
        }
    }

    @Override
    void moveRight() {
        vx = gndvx + vr;
        Right = true;
        if (vr > 0 && mov_ptr != 1 && pos != FALLING) {
            mov_ptr = 1;
            timer = 0;
        }
    }

    public static void LoadAssets() {
        mov = new Animation[name.length];
        //TextureRegion[] region = TextureManager.getTextures(files);
        TextureRegion[] frames;
        //mov = new Animation[files.length-1];
        for (int i = 0; i < mov.length; i++) {
            frames = TextureManager.getRegion(TextureManager.getRegionIndex(name[i])).split(data[i][0], data[i][1])[0];

            mov[i] = new Animation(data[i][2] / 1000f, frames);
            switch (data[i][3]) {
                case 1: {
                    mov[i].setPlayMode(Animation.PlayMode.LOOP);
                    break;
                }
                case 2: {
                    mov[i].setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
                    break;
                }
                case 3: {
                    mov[i].setPlayMode(Animation.PlayMode.LOOP_RANDOM);
                    break;
                }
                case 4: {
                    mov[i].setPlayMode(Animation.PlayMode.LOOP_REVERSED);
                    break;
                }
                case 5: {
                    mov[i].setPlayMode(Animation.PlayMode.NORMAL);
                    break;
                }
                case 6: {
                    mov[i].setPlayMode(Animation.PlayMode.REVERSED);
                    break;
                }
            }
        }
    }
}
