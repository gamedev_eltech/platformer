package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.CameraManager;
import com.shittygames.managers.TextureManager;

public class Background extends MyActor {
    public static final int CAM_SIZE= 0xffffffff;
    enum DrawMode {PAVE, STRETCH}
    private DrawMode mode = DrawMode.STRETCH;
    private float tileWidth,tileHeight,deltaX=0,deltaY=0;
    private boolean customH=false,customW=false;
    private Parallax parallax=null;
    public Background(String texture) {
        super(0, 0, 0, 0, texture, 1, "back",0);
    }
    public Background(String texture,float dX,float dY,float width,float height,String name,int layer){
        super(0,0,width,height,texture,1,name,layer);
        deltaX =dX;
        deltaY = dY;
        if(width!=CAM_SIZE)customW=true;
        if(height!=CAM_SIZE)customH=true;
    }
    public void setParallax(float parallaxX,float parallaxY){
        this.parallax = new Parallax(parallaxX,parallaxY,CameraManager.GetCamX(),CameraManager.GetCamY());
    }
    public void refreshParallax(){
        this.parallax.startX = CameraManager.GetCamX();
        this.parallax.startY = CameraManager.GetCamY();
    }
    @Override
    public void update(float delta){
        texture.update(delta);
        x = CameraManager.GetCamX()- CameraManager.GetViewportWidth()/2 +deltaX
                + (parallax!=null?parallax.getDeltaX(CameraManager.GetCamX()):0);
        y = CameraManager.GetCamY() - CameraManager.GetViewportHeight()/2 + deltaY
                + (parallax!=null?parallax.getDeltaY(CameraManager.GetCamY()):0);
        width = customW?width:CameraManager.GetViewportWidth();
        height = customH?height:CameraManager.GetViewportHeight();
    }
    public void setPaveMode(float tileWidth, float tileHeight) {//ЗАМОСТИТЬ
        mode = DrawMode.PAVE;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    public void setStretchMode() { //РАСТЯНУТЬ
        mode = DrawMode.STRETCH;
    }
    @Override
    public void draw(SpriteBatch batch) {
        if (mode == DrawMode.STRETCH) batch.draw(texture.getFrame(), x, y, width, height);
        else {
            int row = (int) (height / tileHeight), col = (int) (width / tileWidth);
            row += (row * tileHeight < height) ? 1 : 0;
            col += (col * tileWidth < width) ? 1 : 0;
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    batch.draw(texture.getFrame(),
                            x + tileWidth * j,
                            y + tileHeight * i,
                            tileWidth * (j + 1) > width ? tileWidth * (j+1) - width : tileWidth,
                            tileHeight * (i + 1) > height ? tileHeight * (i+1) - height : tileHeight);
                }

            }
        }
        /*if(x+width<CameraManager.GetCamX()+CameraManager.GetViewportWidth()/2){x+=width;this.draw(batch);x-=width;}
        else if(x>CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2){x-=width;this.draw(batch);x+=width;}
        if(y+height<CameraManager.GetCamY()+CameraManager.GetViewportHeight()/2){y+=height;this.draw(batch);y-=height;}
        else if(y>CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2){y-=height;this.draw(batch);y+=height;}
        */
    }
    private static class Parallax{
        float parallaxX,parallaxY;
        float startX,startY;
        public Parallax(float parallaxX,float parallaxY,float startX,float startY){
            this.parallaxX = parallaxX;
            this.parallaxY = parallaxY;
            this.startX = startX;
            this.startY = startY;
        }
        public float getDeltaX(float x){
            return parallaxX*(startX-x);
        }
        public float getDeltaY(float y){
            return parallaxY*(startY-y);
        }
    }
}
