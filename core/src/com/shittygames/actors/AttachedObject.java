package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.TextureManager;

public class AttachedObject extends MyActor {
    private float angle=0,dX=0,dY=0;
    MyActor origin;
    public AttachedObject(float dX, float dY, float width, float height, String texture, int script, String name, int layer) {
        super(0, 0, width, height, texture, script, name, layer);
        this.dX = dX;
        this.dY = dY;
    }
    public void attach(MyActor origin){
        this.origin = origin;
    }
    public void setPos(float dX,float dY){this.dX=dX;this.dY=dY;}
    public void setAngle(float angle){this.angle = angle;}
    public void addAngle(float angle){this.angle+=angle;}
    public float getAngle(){return angle;}
    @Override
    public float getX(){
        return origin.getX()+dX;
    }
    @Override
    public float getY(){
        return origin.getY()+dY;
    }
    @Override
    public void update(float delta){
        texture.update(delta);
        this.setX(origin.getX()+dX);
        this.setY(origin.getY()+dY);
    }
    @Override
    public void draw(SpriteBatch batch){
        batch.draw(texture.getFrame(),flipX?x-width:x,flipY?y-height:y,
                width/2,height/2,
                width,height,flipX?-1:1,flipY?-1:1,angle);
    }
}
