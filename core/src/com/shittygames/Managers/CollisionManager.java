package com.shittygames.Managers;

import com.shittygames.actors.Explosion;
import com.shittygames.actors.Mobs.Mob;
import com.shittygames.actors.Mobs.Mob.*;
import com.shittygames.actors.MyActor;

/*
 * Created by user-00 on 15.09.17.
 * 0 - absolute wall
 * 1 - decorations - never calls if main
 * 2 - static box
 * 3 - explosion
 * 4 - movable obj
 * 5 - mob
 * 6 - player
 * 7 - attack box - enemy
 * 8 - attack box - player
 */
public class CollisionManager {
    public static void ReactOn(MyActor MainActor, MyActor CollidedObj, byte posInfo) {
        //posInfo : 0 - main upper than obj
        //          1 -  right
        //          2 - below
        //          3 - left
        //          4 - main inside the obj
        //          5 - obj inside the main
        switch (CollidedObj.getScript()){
            case 0:{
                MainActor.vx = 0;
                if (posInfo == 0) {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) {((Mob) MainActor).gndvx = CollidedObj.vx;
                    ((Mob) MainActor).pos = airPos.GROUNDED;}
                    MainActor.vy = 0;
                    MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
                } else if (posInfo == 1) {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) ((Mob) MainActor).vl = 0;
                    MainActor.vx = (MainActor.vx < 0 ? 0 : MainActor.vx);
                    MainActor.setX(CollidedObj.getX() + CollidedObj.getWidth());
                } else if (posInfo == 2) {
                    MainActor.vy = MainActor.vy > 0 ? 0 : MainActor.vy;
                    MainActor.setY(CollidedObj.getY() - MainActor.getHeight() - 1);

                } else {
                    if (MainActor.getScript() == 5|| MainActor.getScript()==6) ((Mob) MainActor).vr = 0;
                    MainActor.vx = (MainActor.vx > 0 ? 0 : MainActor.vx);
                    MainActor.setX(CollidedObj.getX() - MainActor.getWidth());
                }
                break;
            }
            case 2:{
                MainActor.vx = 0;
                if(posInfo==0&&MainActor.vy<0){
                    MainActor.vy = 0;
                    MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
                    if(MainActor.getScript() == 5|| MainActor.getScript()==6){
                    ((Mob) MainActor).gndvx = CollidedObj.vx;
                    ((Mob) MainActor).pos = airPos.GROUNDED;
                    }
                }
                break;
            }
            case 3:{
                if((((Explosion)CollidedObj).isDamages)&&(MainActor.getScript()==6||MainActor.getScript()==5)){((Mob)MainActor).HP-=10;
                CollidedObj.Destroy();}
                break;
            }
            case 4:{

            }
            case 5:{

            }
            case 6:{

            }
            case 7:{

            }
            case 8:{

            }
        }/*
        if (MainActor.isCollides() && CollidedObj.isCollides()) {
            MainActor.vx =0;
            if (posInfo == 0) {
                if (MainActor.getScript() == 0) ((Mob) MainActor).gndvx = CollidedObj.vx;
                ((Mob) MainActor).pos = airPos.GROUNDED;
                MainActor.vy = 0;
                MainActor.setY(CollidedObj.getY() + CollidedObj.getHeight());
            } else if (posInfo == 1) {
                if (MainActor.getScript() == 0) ((Mob) MainActor).vl = 0;
                MainActor.vx = (MainActor.vx < 0 ? 0 : MainActor.vx);
                MainActor.setX(CollidedObj.getX() + CollidedObj.getWidth());
            } else if (posInfo == 2) {
                MainActor.vy = MainActor.vy > 0 ? 0 : MainActor.vy;
                MainActor.setY(CollidedObj.getY() - MainActor.getHeight() - 1);

            } else {
                if (MainActor.getScript() == 0) ((Mob) MainActor).vr = 0;
                MainActor.vx = (MainActor.vx > 0 ? 0 : MainActor.vx);
                MainActor.setX(CollidedObj.getX() - MainActor.getWidth());
            }
        }
        switch (CollidedObj.getScript()) {
            case 2: {
                if (posInfo == 0) {
                    MainActor.vx = CollidedObj.vx;
                    if (CollidedObj.vy <= 0) MainActor.vy = CollidedObj.vy;
                }
                break;
            }
            case 192:{
                if(MainActor.getScript()==0){
                    //AudioAdapter.playMusic(0);
                    ((Mob)CollidedObj).HP-=1;
                    ((Mob)MainActor).pos = airPos.GROUNDED;
                    ((Mob)MainActor).jump();
                }
            }
        }
         */
    }

}
