package com.shittygames.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by user-00 on 16.09.17.
 */
public class TextureManager {
    private static TextureRegion[] textures;
    private static Integer data[][];
    /*
    data example:
    5 64 20 <- pos params: frames count, frame width, each frame duration *100
    poses: idle, move, jump, fall, attack, hit, death, special1, sit, special2, etc
    */
    private static Scanner dataScanner;
    public static void Initialize() throws FileNotFoundException {
        Scanner scanner;
        try {
            scanner = new Scanner(new FileInputStream("textures.list"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            scanner = null;
        }
        textures = new TextureRegion[scanner.nextInt()];
        data=new Integer[textures.length][];
        File dataF;
        String name;
        ArrayList<Integer> dataBuffer=new ArrayList<>();
        for (int i = 0; i < textures.length; i++) {
            name = scanner.next();
            dataF = new File(name+".data");
            if(dataF.exists()){
                dataScanner = new Scanner(dataF);
                while(dataScanner.hasNext())dataBuffer.add(dataScanner.nextInt());
                dataScanner.close();
                data[i] = dataBuffer.toArray(new Integer[]{});
                dataBuffer.clear();
            }
            else {data[i] = new Integer[]{-1};}
            textures[i] = new TextureRegion(new Texture(name+".png"), scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());

        }
        scanner.close();
    }

    public static TextureRegion getTexture(int texture) {
        return textures[texture];
    }
    public static Integer[] getData(int texture){
        return data[texture];
    }
    public static TextureRegion[] getTextures(String[] files){
        TextureRegion[] regions = new TextureRegion[files.length-1];
        //TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(files[0]+"/"+files[0]+".pack"));
        //for(int i=1;i<files.length;i++){
        //    regions[i-1] = atlas.findRegion(files[i]);
        //}
        //atlas.dispose();
        for(int i=1;i<files.length;i++){
            regions[i-1] = new TextureRegion(new Texture(Gdx.files.internal(files[0]+'/'+files[i]+".png")));
            //System.out.println(regions[i-1].getTexture().toString()+ "is Loaded");
        }
        return regions;
    }
}
