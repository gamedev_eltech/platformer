package com.shittygames.Managers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.actors.*;

import java.util.ArrayList;
import java.util.Comparator;

public class ActorManager {
    private static ArrayList<MyActor> list;
    private static Comparator<MyActor> comparator;

    public static void Initialize() {
        comparator = (o1, o2) -> {
            int o = o1.layer - o2.layer;
            if (o == 0) return 0;
            else if (o > 0) return 1;
            else return -1;
        };
        list = new ArrayList<>();
    }

    public static void Add(MyActor actor) {
    list.add(actor);
    }

    public static void Remove(String name) {
        list.get(Find(name)).Destroy();
    }

    private static int Find(String name) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).toString().equals(name)) return i;
        }
        return -1;
    }
    public static MyActor get(String name){
        return list.get(Find(name));
    }

    public static void Sort() {
        list.sort(comparator);
    }

    public static void Step(SpriteBatch batch, float delta) {
        for(MyActor a: list)a.update(delta);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isDestroying()) {
                list.remove(i);
                i--;
            }
        }
        Sort();
        for(MyActor a: list)a.draw(batch);
        }
    public static void CheckTouches(MyActor actor,float delta){
        for (MyActor a:list) {
            if(a.equals(actor)||a.getScript()==1)continue;
            if (actor.getX() + actor.getWidth() + actor.vx * delta >= a.getX()
                    & actor.getX() + actor.getWidth() <= a.getX() + a.getWidth() / 2 &
                    (actor.getY() + actor.getHeight() >= a.getY() & actor.getY() < a.getY() + a.getHeight())) {
                CollisionManager.ReactOn(actor, a, (byte) 3); //actor left
            } else {
                if (actor.getX() + actor.vx * delta <= a.getX() + a.getWidth()
                        & actor.getX() >= a.getX() + a.getWidth() / 2 &
                        (actor.getY() + actor.getHeight() >= a.getY() & actor.getY() < a.getY() + a.getHeight())) {
                    CollisionManager.ReactOn(actor, a, (byte) 1); //actor right
                }
            }
            if (actor.getY() < a.getY() + a.getHeight() - actor.vy * delta
                    & actor.getY() >= a.getY() + a.getHeight() / 2 & (actor.getX() < a.getX() + a.getWidth()
                    & ((actor.getX() > a.getX()) | (actor.getX() + actor.getWidth() > a.getX())))) {
                CollisionManager.ReactOn(actor, a, (byte) 0); //actor upper
            } else {
                if (actor.getY() + actor.getHeight() >= a.getY() + actor.vy * delta
                        & actor.getY() + actor.getHeight() <= a.getY() + a.getHeight() / 2 &
                        (actor.getX() < a.getX() + a.getWidth()
                                & ((actor.getX() > a.getX()) | (actor.getX() + actor.getWidth() > a.getX())))) {
                    CollisionManager.ReactOn(actor, a, (byte) 2); //actor below
                }
                else{
                    if(
                            (a.getY()>=actor.getY()&&a.getY()+a.getHeight()<=actor.getY()+actor.getHeight())
                            &&((a.getX()+a.getWidth() + a.vx*delta<=actor.getX()+actor.getWidth()
                                &&a.getX()+a.getWidth()/2 >= actor.getX())
                                ||(a.getX() + a.vx*delta >= actor.getX()
                                    && a.getX() + a.getWidth()/2 <= actor.getX() + actor.getWidth()))
                            ||((a.getX()>=actor.getX() && a.getX()+a.getWidth()<=actor.getX() + actor.getWidth())
                                &&((a.getY()+a.getHeight()/2+a.vy*delta <= actor.getY() + actor.getHeight() && a.getY() >= actor.getY())
                                    ||(a.getY()+a.getHeight()<= actor.getY() + actor.getHeight()
                                    && a.getY() +a.getHeight()/2 + a.vy*delta >= actor.getY()))
                                    )
                            ){ //  a inside actor
                        CollisionManager.ReactOn(actor,a,(byte)5);
                    }
                    else {
                        if((actor.getY()>=a.getY()&&actor.getY()+actor.getHeight()<=a.getY()+a.getHeight())
                                &&((actor.getX()+actor.getWidth() + actor.vx*delta<=a.getX()+a.getWidth()
                                &&actor.getX()+actor.getWidth()/2 >= a.getX())
                                ||(actor.getX() + actor.vx*delta >= a.getX()
                                && actor.getX() + actor.getWidth()/2 <= a.getX() + a.getWidth()))
                                ||((actor.getX()>=a.getX() && actor.getX()+actor.getWidth()<=a.getX() + a.getWidth())
                                &&((actor.getY()+actor.getHeight()/2+actor.vy*delta <= a.getY() + a.getHeight()
                                && actor.getY() >= a.getY())
                                ||(actor.getY()+actor.getHeight()<= a.getY() + a.getHeight()
                                && actor.getY() +actor.getHeight()/2 + actor.vy*delta >= actor.getY()))
                        )) { // actor inside a
                        CollisionManager.ReactOn(actor,a,(byte)4);
                        }
                    }
                }
            }
        }
    }

}
