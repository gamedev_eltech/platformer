package com.shittygames.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.shittygames.actors.Explosion;
import com.shittygames.actors.Mobs.Mob;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.Mobs.TestBot;

import static com.shittygames.Managers.ActorManager.get;

public class ScriptManager {
    public static void LoadResources(){
        Player.LoadAssets();
        Explosion.LoadAssets();
        TestBot.LoadAssets();
    }
    public static void UserInput(){
        /*if(Gdx.input.isKeyPressed(Input.Keys.NUM_1))ActorAdapter.Add(new Explosion(400,300,32,32,1,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_2))ActorAdapter.Add(new Explosion(400,300,64,64,2,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_3))ActorAdapter.Add(new Explosion(400,300,128,80,3,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_4))ActorAdapter.Add(new Explosion(400,300,128,128,4,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_5))ActorAdapter.Add(new Explosion(400,300,192,192,5,100,0));
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_6))ActorAdapter.Add(new Explosion(400,300,48,48,6,100,0));
        */
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_1)){
            ActorManager.Add(new Explosion(get("player1").getX(),
                get("player1").getY(),64,64,6,15,1));
            ((Mob)get("player1")).MP-=25;
        }
    }
    public static void ScriptUpdate(){
                if(get("player1").getX()<=-900&&!get("leftwall").isMoving()){
            //System.out.println("call");
            get("leftwall").moveTo(-1050,0,.5f);}
        if(get("player1").getX()>-900&&!get("leftwall").isMoving()){
            get("leftwall").moveTo(-1050,-500,.5f);}
    }
}
