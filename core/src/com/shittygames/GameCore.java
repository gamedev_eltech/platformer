package com.shittygames;

import com.badlogic.gdx.Game;
import com.shittygames.screens.Levels.TestStage;
import com.shittygames.screens.SplashScreen;

public class GameCore extends Game {
    @Override
    public void create() {
        this.setScreen(new TestStage());
        //this.setScreen(new SplashScreen());
    }
}
